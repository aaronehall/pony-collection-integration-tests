# Overview
This repo contains integration tests for the Pony Collection API. The tests validate the interactions between the API and its database.

I re-implemented some of the tests using LightBDD, a lightweight framework that helps enforce a BDD-style (Given/When/Then) format. LightBDD also generates a useful HTML report that is placed in the bin folder after a test run. This can be used to create a product dashboard.

This repo uses the following technologies:

* .NET Core
* XUnit
* Light BDD


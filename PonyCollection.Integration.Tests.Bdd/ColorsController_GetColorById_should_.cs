﻿using System;
using System.Net;
using System.Net.Http;
using LightBDD.Framework;
using LightBDD.Framework.Scenarios.Basic;
using LightBDD.XUnit2;
using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using Xunit;

namespace PonyCollection.Integration.Tests.Bdd
{
    [FeatureDescription(@"
        As a client of the pony collection API,
        I want to be able to retrieve a specific color,
        So that I can know which colors are available to be assigned to my ponies.
    ")]    
    public class ColorsController_GetColorById_should_ 
        : FeatureFixture, IDisposable
    {
        private string _expectedColorName = DataGenerator.String();
        private int _expectedColorId;
        private HttpClient _httpClient = HttpClientFactory.Create();
        private HttpResponseMessage _httpResponseMessage;

        [Scenario]
        public void get_color_by_id()
        {
            Runner.RunScenario(
                Given_an_existing_color,
                When_i_call_the_colors_endpoint_with_colors_id,
                Then_the_api_responds_successfully,
                And_the_api_provides_the_color
            );
        }

        private void Given_an_existing_color()
        {
            _expectedColorId = ColorDatabaseOperations.InsertColorIntoDatabase(_expectedColorName);
        }

        private void When_i_call_the_colors_endpoint_with_colors_id()
        {
            _httpResponseMessage = _httpClient.GetAsync($"colors/{_expectedColorId}").Result;
        }

        private void Then_the_api_responds_successfully()
        {
            Assert.Equal(HttpStatusCode.OK, _httpResponseMessage.StatusCode);
        }

        private void And_the_api_provides_the_color()
        {
            var jsonResponse = _httpResponseMessage.Content.ReadAsStringAsync().Result;
            var actualColor = JsonConvert.DeserializeObject<Color>(jsonResponse);
            Assert.Equal(_expectedColorId, actualColor.Id);
            Assert.Equal(_expectedColorName, actualColor.Name);
        }

        public void Dispose()
        {
            ColorDatabaseOperations.DeleteById(_expectedColorId);
            _httpClient.Dispose();
        }
    }
}

﻿using LightBDD.Framework;
using LightBDD.Framework.Scenarios.Basic;
using LightBDD.XUnit2;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using System;
using System.Net;
using System.Net.Http;
using Xunit;

namespace PonyCollection.Integration.Tests.Bdd
{
    [FeatureDescription(@"
        As a client of the pony collection API,
        I want to be able to delete a color
        So that unnecessary colors won't be displayed in my list
    ")]
    public class ColorsController_Delete_should_ 
        : FeatureFixture, IDisposable
    {
        private int _expectedColorId;
        private string _expectedColorName = DataGenerator.String();
        private HttpClient _httpClient = HttpClientFactory.Create();
        private HttpResponseMessage _httpResponseMessage;

        [Scenario]
        public void delete_a_color()
        {
            Runner.RunScenario(
                Given_a_color,
                When_I_delete_the_color_from_the_colors_endpoint,
                Then_the_api_responds_successfully,
                And_the_color_is_deleted
            );
        }

        private void Given_a_color()
        {
            _expectedColorId = ColorDatabaseOperations.InsertColorIntoDatabase(_expectedColorName);
        }

        private void When_I_delete_the_color_from_the_colors_endpoint()
        {
            _httpResponseMessage = _httpClient.DeleteAsync($"colors/{_expectedColorId}").Result;
        }

        private void Then_the_api_responds_successfully()
        {
            Assert.Equal(HttpStatusCode.NoContent, _httpResponseMessage.StatusCode);
        }

        private void And_the_color_is_deleted()
        {
            var colorExistsInDatabase = ColorDatabaseOperations.DetermineIfColorExistsById(_expectedColorId);
            Assert.False(colorExistsInDatabase);
        }

        public void Dispose()
        {
            ColorDatabaseOperations.DeleteById(_expectedColorId);
            _httpClient.Dispose();
        }
    }
}

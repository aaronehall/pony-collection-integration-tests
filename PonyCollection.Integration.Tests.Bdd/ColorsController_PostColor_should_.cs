﻿using LightBDD.Framework;
using LightBDD.Framework.Scenarios.Basic;
using LightBDD.XUnit2;
using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace PonyCollection.Integration.Tests.Bdd
{
    [FeatureDescription(@"
        As a client of the pony collection API,
        I want to be able to create a new color,
        So that I can accurately assign body colors and mane colors to my ponies
    ")]
    public class ColorsController_PostColor_should_
        : FeatureFixture, IDisposable
    {
        private int _colorId;
        private HttpClient _httpClient = HttpClientFactory.Create();
        private HttpResponseMessage _httpResponseMessage;
        private Color _expectedColor;
        private Color _actualColor;

        [Scenario]
        public void create_a_color()
        {
            Runner.RunScenario(
                Given_a_new_color,
                When_I_post_the_new_color,
                Then_the_api_responds_successfully,
                And_returns_the_new_color
            );
        }

        private void Given_a_new_color()
        {
            _expectedColor = new Color { Name = DataGenerator.String() };
        }

        private void When_I_post_the_new_color()
        {
            var colorJsonObject = JsonConvert.SerializeObject(_expectedColor);
            _httpResponseMessage = _httpClient.PostAsync("colors", new StringContent(colorJsonObject, Encoding.UTF8, "application/json")).Result;
            _actualColor = JsonConvert.DeserializeObject<Color>(_httpResponseMessage.Content.ReadAsStringAsync().Result);
            _colorId = _actualColor.Id;
        }

        private void Then_the_api_responds_successfully()
        {
            Assert.Equal(HttpStatusCode.Created, _httpResponseMessage.StatusCode);
        }

        private void And_returns_the_new_color()
        {
            Assert.Equal(_expectedColor.Name, _actualColor.Name);
        }

        public void Dispose()
        {
            ColorDatabaseOperations.DeleteById(_colorId);
            _httpClient.Dispose();
        }
    }
}

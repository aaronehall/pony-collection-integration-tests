﻿using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using System;
using System.Net;
using System.Net.Http;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class PonyTypesController_Delete_should_ : IDisposable
    {
        private int _ponyTypeId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void delete_a_pony_type()
        {
            var expectedPonyTypeName = DataGenerator.String();

            _ponyTypeId = PonyTypeDatabaseOperations.InsertPonyTypeIntoDatabase(expectedPonyTypeName);

            var httpResponseMessage = _httpClient.DeleteAsync($"ponytypes/{_ponyTypeId}").Result;
            Assert.Equal(HttpStatusCode.NoContent, httpResponseMessage.StatusCode);

            var ponyTypeExistsInDatabase = PonyTypeDatabaseOperations.DetermineIfPonyTypeExistsById(_ponyTypeId);
            Assert.False(ponyTypeExistsInDatabase);
        }

        public void Dispose()
        {
            PonyTypeDatabaseOperations.DeleteById(_ponyTypeId);
        }
    }
}

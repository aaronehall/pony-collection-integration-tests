﻿using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using System;
using System.Net;
using System.Net.Http;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class PoniesController_Delete_should_ : IDisposable
    {
        private int _bodyColorId;
        private int _maneColorId;
        private int _ponyTypeId;
        private int _ponyId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void delete_pony()
        {
            var expectedBodyColorName = DataGenerator.String();
            var expectedManeColorName = DataGenerator.String();
            var expectedPonyTypeName = DataGenerator.String();

            _bodyColorId = ColorDatabaseOperations.InsertColorIntoDatabase(expectedBodyColorName);
            _maneColorId = ColorDatabaseOperations.InsertColorIntoDatabase(expectedManeColorName);
            _ponyTypeId = PonyTypeDatabaseOperations.InsertPonyTypeIntoDatabase(expectedPonyTypeName);

            var expectedPony = new
            {
                Name = DataGenerator.String(),
                BodyColorId = _bodyColorId,
                ManeColorId = _maneColorId,
                PonyTypeId = _ponyTypeId
            };

            _ponyId = PonyDatabaseOperations.InsertPonyIntoDatabase(expectedPony);

            var httpResponseMessage = _httpClient.DeleteAsync($"ponies/{_ponyId}").Result;
            Assert.Equal(HttpStatusCode.NoContent, httpResponseMessage.StatusCode);

            var ponyExistsInDatabase = PonyDatabaseOperations.DetermineIfPonyExistsById(_ponyId);
            Assert.False(ponyExistsInDatabase);
        }

        public void Dispose()
        {
            PonyDatabaseOperations.DeleteById(_ponyId);
            PonyTypeDatabaseOperations.DeleteById(_ponyTypeId);
            ColorDatabaseOperations.DeleteById(_bodyColorId);
            ColorDatabaseOperations.DeleteById(_maneColorId);
            _httpClient.Dispose();
        }
    }
}

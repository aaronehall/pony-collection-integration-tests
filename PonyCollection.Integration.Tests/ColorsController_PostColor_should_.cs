﻿using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using System;
using System.Net.Http;
using System.Text;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class ColorsController_PostColor_should_ : IDisposable
    { 
        private int _colorId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void create_a_color()
        {
            var expectedColor = new { Name = DataGenerator.String() };
            var actualColor = PostColor(expectedColor);
            Assert.Equal(expectedColor.Name, actualColor.Name);
        }

        private Color PostColor(object colorToPost)
        {
            var colorJsonObject = JsonConvert.SerializeObject(colorToPost);
            var jsonResponse = _httpClient.PostAsync("colors", new StringContent(colorJsonObject, Encoding.UTF8, "application/json"))
                .Result.Content.ReadAsStringAsync().Result;
            var actualColor = JsonConvert.DeserializeObject<Color>(jsonResponse);
            _colorId = actualColor.Id;

            return actualColor;
        }

        public void Dispose()
        {
            ColorDatabaseOperations.DeleteById(_colorId);
            _httpClient.Dispose();
        }
    }
}

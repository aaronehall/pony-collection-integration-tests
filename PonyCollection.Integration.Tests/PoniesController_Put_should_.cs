﻿using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class PoniesController_Put_should_ : IDisposable
    {
        private int _bodyColorId;
        private int _maneColorId;
        private int _ponyTypeId;
        private int _expectedPonyId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void update_a_pony()
        {
            var expectedBodyColorName = DataGenerator.String();
            var expectedManeColorName = DataGenerator.String();
            var expectedPonyTypeName = DataGenerator.String();

            _bodyColorId = ColorDatabaseOperations.InsertColorIntoDatabase(expectedBodyColorName);
            _maneColorId = ColorDatabaseOperations.InsertColorIntoDatabase(expectedManeColorName);
            _ponyTypeId = PonyTypeDatabaseOperations.InsertPonyTypeIntoDatabase(expectedPonyTypeName);

            var ponyDatabaseEntity = new
            {
                Name = DataGenerator.String(),
                BodyColorId = _bodyColorId,
                ManeColorId = _maneColorId,
                PonyTypeId = _ponyTypeId
            };

            _expectedPonyId = PonyDatabaseOperations.InsertPonyIntoDatabase(ponyDatabaseEntity);

            var expectedPony = new
            {
                Name = DataGenerator.String(),
                BodyColor = expectedBodyColorName,
                ManeColor = expectedManeColorName,
                PonyType = expectedPonyTypeName
            };

            var expectedPonyJson = JsonConvert.SerializeObject(expectedPony);
            
            var httpResponseMessage = _httpClient.PutAsync($"ponies/{_expectedPonyId}", new StringContent(expectedPonyJson, Encoding.UTF8, "application/json")).Result;
            Assert.Equal(HttpStatusCode.Accepted, httpResponseMessage.StatusCode);

            var jsonResponse = httpResponseMessage.Content.ReadAsStringAsync().Result;
            var actualPony = JsonConvert.DeserializeObject<Pony>(jsonResponse);
            Assert.Equal(_expectedPonyId, actualPony.Id);
            Assert.Equal(expectedPony.Name, actualPony.Name);
            Assert.Equal(expectedPony.BodyColor, actualPony.BodyColor);
            Assert.Equal(expectedPony.ManeColor, actualPony.ManeColor);
            Assert.Equal(expectedPony.PonyType, actualPony.PonyType);
        }

        public void Dispose()
        {
            PonyDatabaseOperations.DeleteById(_expectedPonyId);
            PonyTypeDatabaseOperations.DeleteById(_ponyTypeId);
            ColorDatabaseOperations.DeleteById(_bodyColorId);
            ColorDatabaseOperations.DeleteById(_maneColorId);
            _httpClient.Dispose();
        }
    }
}

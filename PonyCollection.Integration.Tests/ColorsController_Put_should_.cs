﻿using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class ColorsController_Put_should_ : IDisposable
    {
        private int _expectedColorId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void update_color()
        {
            _expectedColorId = ColorDatabaseOperations.InsertColorIntoDatabase(DataGenerator.String());
            var expectedColor = new { Name = DataGenerator.String() };
            var expectedColorJson = JsonConvert.SerializeObject(expectedColor);

            var httpResponseMessage = _httpClient.PutAsync($"colors/{_expectedColorId}", new StringContent(expectedColorJson, Encoding.UTF8, "application/json")).Result;
            Assert.Equal(HttpStatusCode.Accepted, httpResponseMessage.StatusCode);

            var jsonResponse = httpResponseMessage.Content.ReadAsStringAsync().Result;
            var actualColor = JsonConvert.DeserializeObject<Color>(jsonResponse);
            Assert.Equal(_expectedColorId, actualColor.Id);
            Assert.Equal(expectedColor.Name, actualColor.Name);
        }

        public void Dispose()
        {
            ColorDatabaseOperations.DeleteById(_expectedColorId);
            _httpClient.Dispose();
        }
    }
}

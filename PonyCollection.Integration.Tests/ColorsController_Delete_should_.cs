﻿using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using System;
using System.Net;
using System.Net.Http;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class ColorsController_Delete_should_ : IDisposable
    {
        private int _colorId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void delete_a_color()
        {
            var expectedColorName = DataGenerator.String();

            _colorId = ColorDatabaseOperations.InsertColorIntoDatabase(expectedColorName);

            var httpResponseMessage = _httpClient.DeleteAsync($"colors/{_colorId}").Result;
            Assert.Equal(HttpStatusCode.NoContent, httpResponseMessage.StatusCode);

            var colorExistsInDatabase = ColorDatabaseOperations.DetermineIfColorExistsById(_colorId);
            Assert.False(colorExistsInDatabase);
        }

        public void Dispose()
        {
            ColorDatabaseOperations.DeleteById(_colorId);
        }
    }
}

using System.Net.Http;
using System;
using Xunit;
using Newtonsoft.Json;
using System.Net;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;

namespace PonyCollection.Integration.Tests
{
    public class ColorsController_GetColorById_should_ : IDisposable
    {
        private int _expectedColorId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void get_color_by_id()
        {
            var expectedColorName = DataGenerator.String();

            _expectedColorId = ColorDatabaseOperations.InsertColorIntoDatabase(expectedColorName);

            var httpResponseMessage = _httpClient.GetAsync($"colors/{_expectedColorId}").Result;
            Assert.Equal(HttpStatusCode.OK, httpResponseMessage.StatusCode);

            var jsonResponse = httpResponseMessage.Content.ReadAsStringAsync().Result;
            var actualColor = JsonConvert.DeserializeObject<Color>(jsonResponse);
            Assert.Equal(_expectedColorId, actualColor.Id);
            Assert.Equal(expectedColorName, actualColor.Name);
        }

        public void Dispose()
        {
            ColorDatabaseOperations.DeleteById(_expectedColorId);
            _httpClient.Dispose();
        }
    }
}

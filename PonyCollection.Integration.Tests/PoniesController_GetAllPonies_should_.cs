﻿using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class PoniesController_GetAllPonies_should_ : IDisposable
    {
        private int _bodyColorId;
        private int _maneColorId;
        private int _ponyTypeId;
        private int _ponyId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void get_all_ponies()
        {
            var expectedBodyColorName = DataGenerator.String();
            var expectedManeColorName = DataGenerator.String();
            var expectedPonyTypeName = DataGenerator.String();

            _bodyColorId = ColorDatabaseOperations.InsertColorIntoDatabase(expectedBodyColorName);
            _maneColorId = ColorDatabaseOperations.InsertColorIntoDatabase(expectedManeColorName);
            _ponyTypeId = PonyTypeDatabaseOperations.InsertPonyTypeIntoDatabase(expectedPonyTypeName);

            var expectedPony = new
            {
                Name = DataGenerator.String(),
                BodyColorId = _bodyColorId,
                ManeColorId = _maneColorId,
                PonyTypeId = _ponyTypeId
            };

            _ponyId = PonyDatabaseOperations.InsertPonyIntoDatabase(expectedPony);

            var httpResponseMessage = _httpClient.GetAsync($"ponies").Result;
            Assert.Equal(HttpStatusCode.OK, httpResponseMessage.StatusCode);

            var jsonResponse = httpResponseMessage.Content.ReadAsStringAsync().Result;
            var actualPonies = JsonConvert.DeserializeObject<List<Pony>>(jsonResponse);

            Assert.True(actualPonies.Count > 0);
            Assert.Contains(actualPonies, pony => pony?.Name == expectedPony.Name);
        }

        public void Dispose()
        {
            PonyDatabaseOperations.DeleteById(_ponyId);
            PonyTypeDatabaseOperations.DeleteById(_ponyTypeId);
            ColorDatabaseOperations.DeleteById(_bodyColorId);
            ColorDatabaseOperations.DeleteById(_maneColorId);
            _httpClient.Dispose();
        }
    }
}

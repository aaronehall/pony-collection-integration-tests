﻿using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using System;
using System.Net;
using System.Net.Http;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class PonyTypesController_GetPonyTypeById_should_ : IDisposable
    {
        private int _expectedPonyTypeId = 0;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void get_ponyType_by_id()
        {
            var expectedPonyTypeName = DataGenerator.String();

            _expectedPonyTypeId = PonyTypeDatabaseOperations.InsertPonyTypeIntoDatabase(expectedPonyTypeName);

            var httpResponseMessage = _httpClient.GetAsync($"ponytypes/{_expectedPonyTypeId}").Result;
            Assert.Equal(HttpStatusCode.OK, httpResponseMessage.StatusCode);

            var jsonResponse = httpResponseMessage.Content.ReadAsStringAsync().Result;
            var actualPonyType = JsonConvert.DeserializeObject<Color>(jsonResponse);
            Assert.Equal(_expectedPonyTypeId, actualPonyType.Id);
            Assert.Equal(expectedPonyTypeName, actualPonyType.Name);
        }

        public void Dispose()
        {
            PonyTypeDatabaseOperations.DeleteById(_expectedPonyTypeId);
            _httpClient.Dispose();
        }
    }
}

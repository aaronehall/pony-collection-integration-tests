﻿using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using System;
using System.Net.Http;
using System.Text;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class PonyTypesController_PostPonyType_should_ : IDisposable
    {
        private int _ponyTypeId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void post_pony_type()
        {
            var expectedPonyType = new { Name = DataGenerator.String() };
            var actualPonyType = PostPonyType(expectedPonyType);
            Assert.Equal(expectedPonyType.Name, actualPonyType.Name);
        }

        private PonyType PostPonyType(object colorToPost)
        {
            var ponyTypeJsonObject = JsonConvert.SerializeObject(colorToPost);
            var jsonResponse = _httpClient.PostAsync("ponytypes", new StringContent(ponyTypeJsonObject, Encoding.UTF8, "application/json"))
                .Result.Content.ReadAsStringAsync().Result;
            var actualPonyType = JsonConvert.DeserializeObject<PonyType>(jsonResponse);
            _ponyTypeId = actualPonyType.Id;

            return actualPonyType;
        }

        public void Dispose()
        {
            PonyTypeDatabaseOperations.DeleteById(_ponyTypeId);
            _httpClient.Dispose();
        }
    }
}

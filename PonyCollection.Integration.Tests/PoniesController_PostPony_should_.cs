﻿using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using System;
using System.Net.Http;
using System.Text;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class PoniesController_PostPony_should_ : IDisposable
    {
        private int _bodyColorId;
        private int _maneColorId;
        private int _ponyTypeId;
        private int _ponyId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void post_pony()
        {
            var bodyColorName = DataGenerator.String();
            var maneColorName = DataGenerator.String();
            var ponyTypeName = DataGenerator.String();

            _bodyColorId = ColorDatabaseOperations.InsertColorIntoDatabase(bodyColorName);
            _maneColorId = ColorDatabaseOperations.InsertColorIntoDatabase(maneColorName);
            _ponyTypeId = PonyTypeDatabaseOperations.InsertPonyTypeIntoDatabase(ponyTypeName);

            var expectedPony = new
            {
                Name = DataGenerator.String(),
                BodyColor = bodyColorName,
                ManeColor = maneColorName,
                PonyType = ponyTypeName
            };

            var actualColor = PostPony(expectedPony);
            Assert.Equal(expectedPony.Name, actualColor.Name);
            Assert.Equal(expectedPony.BodyColor, actualColor.BodyColor);
            Assert.Equal(expectedPony.ManeColor, actualColor.ManeColor);
            Assert.Equal(expectedPony.PonyType, actualColor.PonyType);
        }

        private Pony PostPony(object ponyToPost)
        {
            var colorJsonObject = JsonConvert.SerializeObject(ponyToPost);
            var jsonResponse = _httpClient.PostAsync("ponies", new StringContent(colorJsonObject, Encoding.UTF8, "application/json"))
                .Result.Content.ReadAsStringAsync().Result;
            var actualPony = JsonConvert.DeserializeObject<Pony>(jsonResponse);
            _ponyId = actualPony.Id;

            return actualPony;
        }

        public void Dispose()
        {
            PonyDatabaseOperations.DeleteById(_ponyId);
            ColorDatabaseOperations.DeleteById(_bodyColorId);
            ColorDatabaseOperations.DeleteById(_maneColorId);
            PonyTypeDatabaseOperations.DeleteById(_ponyTypeId);
            _httpClient.Dispose();
        }
    }
}

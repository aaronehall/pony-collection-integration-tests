﻿using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class ColorsController_GetAllColors_should_ : IDisposable
    {
        private int _colorId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void get_all_colors()
        {
            var expectedColorName = DataGenerator.String();

            _colorId = ColorDatabaseOperations.InsertColorIntoDatabase(expectedColorName);

            var httpResponseMessage = _httpClient.GetAsync($"colors").Result;
            Assert.Equal(HttpStatusCode.OK, httpResponseMessage.StatusCode);

            var jsonResponse = httpResponseMessage.Content.ReadAsStringAsync().Result;
            var actualColors = JsonConvert.DeserializeObject<List<Color>>(jsonResponse);

            Assert.True(actualColors.Count > 0);
            Assert.Contains(actualColors, color => color.Name == expectedColorName);
        }

        public void Dispose()
        {
            ColorDatabaseOperations.DeleteById(_colorId);
            _httpClient.Dispose();
        }
    }
}

﻿using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class PonyTypesController_Put_should_ : IDisposable
    {
        private int _expectedPonyTypeId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void update_pony_type()
        {
            _expectedPonyTypeId = PonyTypeDatabaseOperations.InsertPonyTypeIntoDatabase(DataGenerator.String());
            var expectedPonyType = new { Name = DataGenerator.String() };
            var expectedPonyTypeJson = JsonConvert.SerializeObject(expectedPonyType);

            var httpResponseMessage = _httpClient.PutAsync($"ponytypes/{_expectedPonyTypeId}", new StringContent(expectedPonyTypeJson, Encoding.UTF8, "application/json")).Result;
            Assert.Equal(HttpStatusCode.Accepted, httpResponseMessage.StatusCode);

            var jsonResponse = httpResponseMessage.Content.ReadAsStringAsync().Result;
            var actualPonyType = JsonConvert.DeserializeObject<PonyType>(jsonResponse);
            Assert.Equal(_expectedPonyTypeId, actualPonyType.Id);
            Assert.Equal(expectedPonyType.Name, actualPonyType.Name);
        }

        public void Dispose()
        {
            PonyTypeDatabaseOperations.DeleteById(_expectedPonyTypeId);
            _httpClient.Dispose();
        }
    }
}

﻿using Newtonsoft.Json;
using PonyCollection.Integration.Tests.Infrastructure;
using PonyCollection.Integration.Tests.Infrastructure.Database;
using PonyCollection.Integration.Tests.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Xunit;

namespace PonyCollection.Integration.Tests
{
    public class PonyTypesController_GetAllPonyTypes : IDisposable
    {
        private int _ponyTypeId;
        private HttpClient _httpClient = HttpClientFactory.Create();

        [Fact]
        public void get_all_pony_types()
        {
            var expectedPonyTypeName = DataGenerator.String();

            _ponyTypeId = PonyTypeDatabaseOperations.InsertPonyTypeIntoDatabase(expectedPonyTypeName);

            var httpResponseMessage = _httpClient.GetAsync($"ponytypes").Result;
            Assert.Equal(HttpStatusCode.OK, httpResponseMessage.StatusCode);

            var jsonResponse = httpResponseMessage.Content.ReadAsStringAsync().Result;
            var actualPonyTypes = JsonConvert.DeserializeObject<List<PonyType>>(jsonResponse);

            Assert.True(actualPonyTypes.Count > 0);
            Assert.Contains(actualPonyTypes, ponyType => ponyType.Name == expectedPonyTypeName);
        }

        public void Dispose()
        {
            PonyTypeDatabaseOperations.DeleteById(_ponyTypeId);
            _httpClient.Dispose();
        }
    }
}
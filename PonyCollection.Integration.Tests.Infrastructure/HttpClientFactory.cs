﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace PonyCollection.Integration.Tests.Infrastructure
{
    public class HttpClientFactory
    {
        public static HttpClient Create()
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("http://localhost:49944/api/")
            };

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return httpClient;
        }
    }
}

﻿using MySql.Data.MySqlClient;

namespace PonyCollection.Integration.Tests.Infrastructure.Database
{
    public class DbConnectionFactory
    {
        public static MySqlConnection Create()
        {
            var connectionStringBuilder = new MySqlConnectionStringBuilder
            {
                Server = "127.0.0.1",
                UserID = "root",
                Password = "password",
                Port = 3306,
                Database = "PonyCollection"
            };

            return new MySqlConnection(connectionStringBuilder.ToString());
        }
    }
}

﻿using System;

namespace PonyCollection.Integration.Tests.Infrastructure.Database
{
    public class ColorDatabaseOperations : DatabaseOperationsBase
    {
        public static void DeleteById(int id)
        {
            var commandText = $"DELETE FROM Color WHERE Id = {id}";
            ExecuteNonQuery(commandText);
        }

        public static int InsertColorIntoDatabase(string colorName)
        {
            var commandText = $"INSERT INTO Color SET Name = '{colorName}'; SELECT LAST_INSERT_ID();";
            return Convert.ToInt32(ExecuteScalar(commandText));
        }

        public static bool DetermineIfColorExistsById(int id)
        {
            var commandText = $"SELECT COUNT(1) FROM Color WHERE Id = {id}";
            return Convert.ToInt32(ExecuteScalar(commandText)) > 0;
        }
    }
}

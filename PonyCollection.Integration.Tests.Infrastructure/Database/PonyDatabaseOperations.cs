﻿using System;

namespace PonyCollection.Integration.Tests.Infrastructure.Database
{
    public class PonyDatabaseOperations : DatabaseOperationsBase
    {
        private const string InsertPony = @"
        INSERT INTO Pony
        (
	        Name,
	        BodyColorId,
	        ManeColorId,
	        PonyTypeId
        )
        VALUES
        (
	        @Name,
	        @BodyColorId,
	        @ManeColorId,
	        @PonyTypeId
        );

        SELECT LAST_INSERT_ID();";

        public static int InsertPonyIntoDatabase(object expectedPony) => Convert.ToInt32(ExecuteScalar(InsertPony, expectedPony));

        public static void DeleteById(int id)
        {
            var commandText = $"DELETE FROM Pony WHERE Id = {id}";
            ExecuteNonQuery(commandText);
        }

        public static bool DetermineIfPonyExistsById(int id)
        {
            var commandText = $"SELECT COUNT(1) FROM Pony WHERE Id = {id}";
            return Convert.ToInt32(ExecuteScalar(commandText)) > 0;
        }
    }
}

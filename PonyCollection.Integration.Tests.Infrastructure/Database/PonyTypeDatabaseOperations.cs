﻿using System;

namespace PonyCollection.Integration.Tests.Infrastructure.Database
{
    public class PonyTypeDatabaseOperations : DatabaseOperationsBase
    {
        public static void DeleteById(int id)
        {
            var commandText = $"DELETE FROM PonyType WHERE Id = {id}";
            ExecuteNonQuery(commandText);
        }

        public static int InsertPonyTypeIntoDatabase(string ponyTypeName)
        {
            var commandText = $"INSERT INTO PonyType SET Name = '{ponyTypeName}'; SELECT LAST_INSERT_ID();";
            return Convert.ToInt32(ExecuteScalar(commandText));
        }

        public static bool DetermineIfPonyTypeExistsById(int id)
        {
            var commandText = $"SELECT COUNT(1) FROM PonyType WHERE Id = {id}";
            return Convert.ToInt32(ExecuteScalar(commandText)) > 0;
        }
    }
}

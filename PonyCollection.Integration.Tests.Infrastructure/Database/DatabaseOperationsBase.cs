﻿using Dapper;

namespace PonyCollection.Integration.Tests.Infrastructure.Database
{
    public class DatabaseOperationsBase
    {
        protected static int ExecuteNonQuery(string commandText)
        {
            using (var connection = DbConnectionFactory.Create())
            {
                return connection.Execute(commandText);
            }
        }

        protected static object ExecuteScalar(string commandText, object parameters = null)
        {
            using (var connection = DbConnectionFactory.Create())
            {
                return connection.ExecuteScalar(commandText, parameters);
            }
        }
    }
}

﻿namespace PonyCollection.Integration.Tests.Infrastructure.Entities
{
    public class EntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

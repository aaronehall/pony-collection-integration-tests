﻿namespace PonyCollection.Integration.Tests.Infrastructure.Entities
{
    public class Pony : EntityBase
    {
        public string BodyColor { get; set; }
        public string ManeColor { get; set; }
        public string PonyType { get; set; }
    }
}

﻿using System;
using System.Text;

namespace PonyCollection.Integration.Tests.Infrastructure
{
    public static class DataGenerator
    {
        private static readonly Random _random = new Random();

        public static string String(int length = 10)
        {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var stringBuilder = new StringBuilder();

            for (var i = 0; i < length; i++)
            {
                stringBuilder.Append(chars[_random.Next(chars.Length)]);
            }

            return stringBuilder.ToString();
        }
    }
}
